```sh
# Install bower package manager
$ npm install -g bower

# Install grunt build tool
$ npm install -g grunt-cli

# Install additional grunt tasks
$ npm install

# Install client-side packages
$ bower install
```
