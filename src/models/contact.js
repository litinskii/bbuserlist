define([
    'jquery',
    'backbone'
], function ($, Backbone) {
    'use strict';

    var Contact = Backbone.Model.extend({
        defaults: {
            name: null,
            phone: null,
            email: null
        },
        validation: {
            name: {
                required: true,
                msg: 'Please enter name of user'
            },
            'phone': {
                required: true,
                msg: 'Please enter an phone number'
            },
            email: [{
                required: true,
                msg: 'Please enter an email address'
            }, {
                pattern: 'email',
                msg: 'Please enter a valid email'
            }]
        }
    });

    return Contact;
});