define([
    'jquery',
    'backbone',
    'views/app',
    'router',
    'collections/contacts'
], function ($, Backbone, AppView, Router, ContactsCollection) {
    'use strict';
    var initialize = function () {
        var contactsCollections = new ContactsCollection();
        var appView = new AppView();
        App.router = new Router({view: appView, collection: contactsCollections});
        Backbone.history.start();
    };

    return {
        initialize: initialize
    };
});