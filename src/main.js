define(['app', 'bootstrap'],
    function (MainApp) {
        /*global window*/
        'use strict';

        // Boot the app
        window.App = {};
        MainApp.initialize();

    });