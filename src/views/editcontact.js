define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'backbonevalidation'
], function ($, _, Backbone, Templates) {

    'use strict';
    var EditContactView = Backbone.View.extend({
        template: Templates['editcontact.html'],

        initialize: function () {
            Backbone.Validation.bind(this);
            this.listenTo(this.model, 'validated:invalid', function (model, error) {
                this.cleanFormErrors();
                console.log(model, error);
                _.each(error, this.showFormErrors, this);
            });

        },

        events: {
            'submit .contact-form': 'onFormSubmit',
            'click .btn-close-form': 'onFormClose',
        },

        render: function () {
            this.$el.empty();
            var html = this.template(_.extend(this.model.toJSON(), {
                isNew: this.model.isNew()
            }));
            this.$el.append(html);
            return this;
        },
        onFormSubmit: function (e) {
            e.preventDefault();
            var attrs = {
                name: this.$('.contact-name-input').val(),
                phone: this.$('.contact-phone-input').val(),
                email: this.$('.contact-email-input').val()
            };

            if (this.model.isNew()) {
                var error = this.model.validate(attrs);
                if (error) {
                    this.cleanFormErrors();
                    _.each(error, this.showFormErrors, this);
                    return;
                }
            }
            this.trigger('form:submitted', attrs);
        },

        showFormErrors: function (error, field) {
            this.$('.form-group-' + field).addClass('has-error').find('.help-block').html(error);
        },

        cleanFormErrors: function () {
            this.$('.form-group').removeClass('has-error');
            this.$('.help-block').html('');
        },

        onFormClose: function (e) {
            e.preventDefault();
            this.trigger('form:close');
        }
    });

    return EditContactView;
});