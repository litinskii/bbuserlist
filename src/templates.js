define(['handlebars'], function(Handlebars) {

this["JST"] = this["JST"] || {};

this["JST"]["contact.html"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div class=\"media-heading\">\n    <h3>\n        ";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n    </h3>\n</div>\n<div class=\"media-body\">\n    <dl>\n        <dt>Phone Number:</dt>\n        <dd>";
  if (stack1 = helpers.phone) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</dd>\n        <dt>Email:</dt>\n        <dd>";
  if (stack1 = helpers.email) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.email; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</dd>\n    </dl>\n</div>\n<div class=\"contact-btn-wrapper\">\n    <a href=\"#contacts/edit/";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"edit-contact btn btn-outline\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</a>\n    <a href=\"#contacts/delete/";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"delete-contact btn btn-outline\">\n        <span class=\"glyphicon glyphicon-trash\"></span> Delete\n    </a>\n</div>\n<hr/>";
  return buffer;
  });

this["JST"]["contacts.html"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"row well\">\n    <div class=\"text-center col-sm-6\">\n        <a href=\"#contacts/new\" class=\"btn btn-lg btn-outline\">Add Contact</a>\n    </div>\n    <div class=\"text-center col-sm-6\">\n        <input type=\"text\" class=\"form-control contact-name-search\" placeholder=\"Search\">\n    </div>\n</div>\n<ul class=\"media-list row contacts-container\"></ul>\n<div class=\"empty-contacts-placeholder\"></div>\n<div class=\"empty-search-contacts-placeholder\"></div>";
  });

this["JST"]["editcontact.html"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, self=this, functionType="function", escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  
  return " Create ";
  }

function program3(depth0,data) {
  
  
  return " Edit ";
  }

  buffer += "<h2 class=\"page-header text-center\">";
  stack1 = helpers['if'].call(depth0, depth0.isNew, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " Contact</h2>\n<form role=\"form\" class=\"form-horizontal contact-form\">\n    <div class=\"form-group has-feedback form-group-name\">\n        <label class=\"col-sm-4 control-label\">Full name:</label>\n\n        <div class=\"col-sm-6\">\n            <input type=\"text\" name=\"name\" class=\"form-control contact-name-input\" value=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n            <small class=\"help-block\"></small>\n        </div>\n    </div>\n    <div class=\"form-group has-feedback form-group-email\">\n        <label class=\"col-sm-4 control-label\">Email address:</label>\n\n        <div class=\"col-sm-6\">\n            <input name=\"email\" type=\"email\" class=\"form-control contact-email-input\" value=\"";
  if (stack1 = helpers.email) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.email; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n            <small class=\"help-block\"></small>\n            <small data-error></small>\n        </div>\n    </div>\n    <div class=\"form-group has-feedback form-group-phone\">\n        <label class=\"col-sm-4 control-label\">Telephone number:</label>\n\n        <div class=\"col-sm-6\">\n            <input name=\"phone\" type=\"tel\" class=\"form-control contact-phone-input\" value=\"";
  if (stack1 = helpers.phone) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n            <small class=\"help-block\"></small>\n        </div>\n    </div>\n    <div class=\"form-group\">\n        <div class=\"col-sm-offset-4 col-sm-3\">\n            <button type=\"submit\" class=\"btn btn-outline btn-lg btn-block\">Submit</button>\n        </div>\n        <div class=\"col-sm-3\">\n            <button class=\"btn-close-form btn btn-outline btn-lg btn-block\">Cancel</button>\n        </div>\n    </div>\n</form>";
  return buffer;
  });

return this["JST"];

});